# Proyecto node typescript implementando principios de CQRS 
## ¿Que son los workspaces?
Los `workspaces` (espacios de trabajo) son una característica proporcionada por herramientas como Yarn y npm 
que permiten gestionar múltiples paquetes dentro de un mismo repositorio como un solo proyecto. 
Esta característica es especialmente útil para crear monorepositorios multipaquete y para el desarrollo de microservicios.
### Gestión de dependencias compartidas
Los workspaces permiten compartir dependencias entre múltiples paquetes dentro de un monorepo. 
Esto significa que puedes especificar las dependencias una sola vez en el nivel superior del monorepo 
y todos los paquetes internos utilizarán las mismas versiones de las dependencias compartidas. 
Esto simplifica la gestión de dependencias y reduce la duplicación de paquetes.
### Versionamiento coherente
Al utilizar workspaces, puedes garantizar que todas las dependencias dentro de tu monorepo estén utilizando 
versiones coherentes entre sí. Esto ayuda a evitar problemas de compatibilidad y garantiza que tus paquetes funcionen bien juntos.
### Compartir código y recursos
Los workspaces facilitan el compartir código y otros recursos entre los diferentes paquetes dentro del monorepo. Puedes crear módulos compartidos, bibliotecas de utilidades y componentes reutilizables que sean accesibles desde todos los paquetes en el monorepo.
### Gestión centralizada
Al tener todos tus paquetes en un solo repositorio, la gestión del código fuente, las pruebas, 
las compilaciones y el despliegue se simplifican significativamente. Puedes utilizar herramientas de automatización 
para ejecutar tareas en todos los paquetes de manera coordinada.
### Facilita la implementación de microservicios
Los monorepos multipaquete pueden utilizarse como la base para sistemas basados en microservicios. 
Cada paquete en el monorepo puede representar un servicio individual, y los workspaces facilitan la gestión de la dependencia 
y el despliegue de estos servicios de manera coherente.
## Creando un proyecto node
### Iniciación de un Proyecto Node.js
Para iniciar un proyecto Node.js, sigue estos pasos:

1. Abre tu terminal.
2. En la `carpeta raiz` y en las `subcarpetas` que seran los `subproyectos paketes o modulos` ejecutar el comando.
```bash
npm init -y
```
### Instalación e Iniciación de un Proyecto TypeScript

1. Instala TypeScript como una dependencia de desarrollo en la carpeta raiz ejecutando el siguiente comando:
```bash
npm i typescript -D
```

2. Crea un archivo `tsconfig.json` en la raíz de tu proyecto para configurar TypeScript. Puedes hacerlo ejecutando:
```bash
npx tsc --init
```
### Instalación de ts-node y nodemon

1. Instala `ts-node` para poder ejecutar y depurar archivos TS directamente en Node ejecutando el siguiente comando:
```bash
npm i -D ts-node
```

2. Instala `nodemon` ejecutando el siguiente comando:
```bash
npm i nodemon -D
```
### comandos en pnpm
1. para `--workspace = <package>`
- El `--workspace` es una opción utilizada en el contexto de los comandos de npm para especificar en qué espacio de trabajo (o proyecto) se debe ejecutar el comando. Esto es especialmente útil cuando estás trabajando con monorepos
```bash
npm run dev --workspace=command-bus
```

2. para `npm run --prefix <package> <command>`
- La opción --prefix se utiliza para especificar una ruta base desde la cual se buscará el archivo package.json para ejecutar los scripts.
-  Cuando se usa --prefix, npm buscará el archivo package.json en la ruta especificada y ejecutará los scripts definidos en ese archivo como si estuviera en el directorio raíz del proyecto.

```bash
npm run --prefix cqrs/event-bus dev
```

### configuracion del monorepositorio
1. Primeramente en `"workspaces":` colocar en el arreglo los subproyectos
```json
{
  "name": "cqrs-patrones-ts",
  "private": true,
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "workspaces": [
    "cqrs/**"
  ],
  "scripts": {
    "build:cqrs": "npm run build --workspaces",
    "start:cqrs": "npm run start --workspaces",
    "start2:cqrs": "npm run start --workspaces",
    "dev:cqrs": "npm run dev --workspaces",
    "dev:command": "npm run dev --workspace=command-bus",
    "dev:query": "npm run dev --workspace=query-bus",
    "dev:event": "npm run --prefix cqrs/event-bus dev",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    
  },
  "dependencies": {
  }
}
```
- `npm` no ejecuta scripts en paralelo por defecto. Para ejecutar múltiples scripts en paralelo, puedes usar una herramienta como concurrently.
```bash
npm install concurrently --save-dev
```
```json
{
  "name": "cqrs-patrones-ts",
  "private": true,
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "workspaces": [
    "cqrs/**"
  ],
  "scripts": {
    "build:cqrs": "npm run build --workspaces",
    "start:cqrs": "npm run start --workspaces",
    "start2:cqrs": "npm run start --workspaces",
    "dev:cqrs": "concurrently \"npm run dev:command\" \"npm run dev:query\" \"npm run dev:event\"",
    "dev:command": "npm run dev --workspace=command-bus",
    "dev:query": "npm run dev --workspace=query-bus",
    "dev:event": "npm run --prefix cqrs/event-bus dev",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "concurrently": "^7.0.0"
  },
  "dependencies": {}
}
```
- otra forma de hacerlo es usar lerna
1. Instalar Lerna
```bash
npm install --global lerna
```

2. Inicializar Lerna en tu proyecto, Esto creará un archivo lerna.json en la raíz de tu proyecto.
```bash
lerna init
```

`lerna.json`

```json
{
  "packages": [
    "cqrs/*"
  ],
  "version": "0.0.0"
}
```

`package.json` de la raíz
```json
{
  "name": "cqrs-patrones-ts",
  "private": true,
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "workspaces": [
    "cqrs/*"
  ],
  "scripts": {
    "build:cqrs": "lerna run build --parallel",
    "start:cqrs": "lerna run start --parallel",
    "dev:cqrs": "lerna run dev --parallel",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "lerna": "^7.0.0"
  },
  "dependencies": {}
}
```
- `lerna.json:` Configura Lerna para manejar los paquetes dentro de `cqrs/*.`
- `package.json:` Usa los comandos de Lerna para ejecutar los scripts `build`, `start` y `dev` en todos los paquetes. La opción `--parallel` asegura que los scripts se ejecuten en paralelo.