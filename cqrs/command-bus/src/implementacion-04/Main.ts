import { CommandBus } from "./command/CommandBus";
import { CreateUserCommand } from "./CreateUserCommand";
import { CreateUserCommandHandler } from "./CreateUserCommandHandler";

// Ejemplo de uso
async function main() {
  // Creamos una instancia del Command Bus
  const commandBus = new CommandBus();

  // Registramos el handler para el comando de crear usuario
  commandBus.registerHandler(new CreateUserCommandHandler());

  try {
    // Creamos un comando para crear un usuario
    const createUserCommand = new CreateUserCommand(
      "john_doe",
      "john@example.com"
    );

    // Ejecutamos el comando
    await commandBus.executeCommand(createUserCommand);
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
