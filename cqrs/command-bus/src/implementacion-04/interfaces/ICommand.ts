// Definimos una interfaz para los comandos
export interface ICommand {
  execute(): Promise<void>;
}