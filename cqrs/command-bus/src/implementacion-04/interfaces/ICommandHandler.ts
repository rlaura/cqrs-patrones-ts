import { ICommand } from "./ICommand";

/**
 * Interfaz que define un manejador de comandos.
 * @template T - Tipo de comando que maneja el manejador.
 */
export interface ICommandHandler<T extends ICommand> {
  /**
   * Maneja un comando.
   * @param command El comando que se va a manejar.
   * @returns Una promesa que se resuelve cuando el comando se ha manejado con éxito.
   */
  handle(command: T): Promise<void>;
}
