import { MethodNameInflector } from "./MethodNameInflector";

/**
 * Maneja el comando llamando al método mágico __invoke. Útil para clases
 * de uso único o cierres.
 */
export class Invoke implements MethodNameInflector {
  /**
   * Devuelve el nombre del método mágico __invoke para manejar el comando.
   * 
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método mágico __invoke para manejar el comando.
   */
  getMethodName(commandClassName: string): string {
      return '__invoke';
  }
}