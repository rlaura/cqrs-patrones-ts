import { LastPartOfClassName } from "./LastPartOfClassName";
import { MethodNameInflector } from "./MethodNameInflector";

/**
 * Supone que el método es "handle" + la última parte del nombre de la clase.
 *
 * Ejemplos:
 *  - \MyGlobalCommand              => $handler->handleMyGlobalCommand()
 *  - \My\App\TaskCompletedCommand  => $handler->handleTaskCompletedCommand()
 */
export class HandleLastPartOfClassName implements MethodNameInflector {
  private lastPartOfClassName: LastPartOfClassName;

  constructor() {
    this.lastPartOfClassName = new LastPartOfClassName();
  }

  /**
   * Devuelve el nombre del método para manejar el comando basado en la última parte del nombre de la clase.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método para manejar el comando.
   */
  getMethodName(commandClassName: string): string {
    let commandName = this.lastPartOfClassName.getMethodName(commandClassName);

    return (
      "handle" + commandName.charAt(0).toUpperCase() + commandName.slice(1)
    );
  }
}
