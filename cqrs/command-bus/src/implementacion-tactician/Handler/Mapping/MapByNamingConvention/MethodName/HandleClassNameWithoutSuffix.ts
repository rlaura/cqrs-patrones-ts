import { HandleLastPartOfClassName } from "./HandleLastPartOfClassName";
import { MethodNameInflector } from "./MethodNameInflector";

/**
 * Devuelve un nombre de método que es "handle" + la última parte del nombre de la clase,
 * pero también sin un sufijo dado, típicamente "Command". Esto permite manejar múltiples
 * comandos en un solo objeto pero con nombres de método ligeramente menos molestos.
 *
 * La eliminación de la cadena es sensible a mayúsculas y minúsculas.
 *
 * Ejemplos:
 *  - \CompleteTaskCommand     => $handler->handleCompleteTask()
 *  - \My\App\DoThingCommand   => $handler->handleDoThing()
 */
export class HandleClassNameWithoutSuffix implements MethodNameInflector {
  private suffix: string;
  private suffixLength: number;
  private handleLastPartOfClassName: HandleLastPartOfClassName;

  /**
   * @param suffix La cadena a eliminar del final de cada nombre de clase
   */
  constructor(suffix: string = "Command") {
    this.suffix = suffix;
    this.suffixLength = suffix.length;
    this.handleLastPartOfClassName = new HandleLastPartOfClassName();
  }

  /**
   * Devuelve el nombre del método para manejar el comando sin el sufijo especificado.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método para manejar el comando.
   */
  getMethodName(commandClassName: string): string {
    let methodName =
      this.handleLastPartOfClassName.getMethodName(commandClassName);

    if (methodName.substr(-this.suffixLength) !== this.suffix) {
      return methodName;
    }

    return methodName.substr(0, methodName.length - this.suffixLength);
  }
}
