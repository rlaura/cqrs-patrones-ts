import { MethodNameInflector } from "./MethodNameInflector";

/**
 * Maneja el comando llamando al método "handle".
 */
export class Handle implements MethodNameInflector {
  /**
   * Devuelve el nombre del método que debe llamarse para manejar el comando.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método para manejar el comando.
   */
  getMethodName(commandClassName: string): string {
    return "handle";
  }
}
