import { MethodNameInflector } from "./MethodNameInflector";

/**
 * Supone que el método es solo la última parte del nombre de la clase.
 * 
 * Ejemplos:
 *  - \MyGlobalCommand    => $handler->myGlobalCommand()
 *  - \My\App\CreateUser  => $handler->createUser()
 */
export class LastPartOfClassName implements MethodNameInflector {
  /**
   * Devuelve el nombre del método como la última parte del nombre de la clase.
   * 
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método.
   */
  getMethodName(commandClassName: string): string {
      const lastNamespaceSeparatorPosition = commandClassName.lastIndexOf('\\');

      // Si el nombre de la clase tiene un separador de espacio de nombres, solo tomar la última parte
      if (lastNamespaceSeparatorPosition !== -1) {
          commandClassName = commandClassName.substring(lastNamespaceSeparatorPosition + 1);
      }

      return commandClassName[0].toLowerCase() + commandClassName.substring(1);
  }
}