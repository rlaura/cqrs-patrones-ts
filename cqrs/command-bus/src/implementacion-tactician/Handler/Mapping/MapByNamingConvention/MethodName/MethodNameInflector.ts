/**
 * Deduce el nombre del método a llamar en el manejador de comandos basado en
 * las clases de comando y manejador.
 */
export interface MethodNameInflector {
  /**
   * Devuelve el nombre del método a llamar en el manejador de comandos.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre del método a llamar en el manejador de comandos.
   */
  getMethodName(commandClassName: string): string;
}
