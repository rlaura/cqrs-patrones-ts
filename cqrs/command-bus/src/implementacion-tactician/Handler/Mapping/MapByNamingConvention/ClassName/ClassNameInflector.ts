/**
 * Extrae el nombre de un comando para que el nombre se pueda determinar
 * por el contexto mejor que simplemente el nombre de la clase.
 */
export interface ClassNameInflector {
  /**
   * Deduce el FQCN del Controlador basándose en el FQCN del comando.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre de la clase del controlador.
   */
  getClassName(commandClassName: string): string;
}
