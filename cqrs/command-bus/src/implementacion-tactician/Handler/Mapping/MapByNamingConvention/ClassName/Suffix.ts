import { ClassNameInflector } from "./ClassNameInflector";

/**
 * Implementación de ClassNameInflector que añade un sufijo al nombre de la clase del comando.
 */
export class Suffix implements ClassNameInflector {
  private suffix: string;

  /**
   * Crea una nueva instancia de Suffix con el sufijo dado.
   *
   * @param suffix El sufijo que se agregará al nombre de la clase del comando.
   */
  constructor(suffix: string) {
    this.suffix = suffix;
  }

  /**
   * Devuelve el nombre de la clase del controlador añadiendo el sufijo al nombre de la clase del comando.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns El nombre de la clase del controlador con el sufijo añadido.
   */
  getClassName(commandClassName: string): string {
    return commandClassName + this.suffix;
  }
}
