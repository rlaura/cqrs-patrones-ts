import { MethodToCall } from "../MethodToCall";
import { ClassNameInflector } from "./ClassName/ClassNameInflector";
import { MethodNameInflector } from "./MethodName/MethodNameInflector";

/**
 * El mapeo más común que verás. Pasa un par de inflectores y mapea automáticamente
 * tus comandos a la clase de nombre similar.
 */
export class MapByNamingConvention implements CommandToHandlerMapping {
  private classNameInflector: ClassNameInflector;
  private methodNameInflector: MethodNameInflector;

  /**
   * Crea una nueva instancia de MapByNamingConvention.
   *
   * @param classNameInflector Un objeto que infiere el nombre de la clase del controlador.
   * @param methodNameInflector Un objeto que infiere el nombre del método del controlador.
   */
  constructor(
    classNameInflector: ClassNameInflector,
    methodNameInflector: MethodNameInflector
  ) {
    this.classNameInflector = classNameInflector;
    this.methodNameInflector = methodNameInflector;
  }

  /**
   * Encuentra el manejador para el comando dado.
   *
   * @param commandFQCN El FQCN (Fully Qualified Class Name) del comando.
   * @returns Un objeto que describe el método a llamar en el controlador.
   */
  findHandlerForCommand(commandFQCN: string): MethodToCall {
    return new MethodToCall(
      this.classNameInflector.getClassName(commandFQCN),
      this.methodNameInflector.getMethodName(commandFQCN)
    );
  }
}
