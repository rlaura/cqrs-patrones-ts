/**
 * Excepción lanzada cuando no se puede mapear un comando.
 */
export class FailedToMapCommand extends Error {
  /**
   * Crea una nueva instancia de FailedToMapCommand para la clase de comando dada.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns Una nueva instancia de FailedToMapCommand.
   */
  static className(commandClassName: string): FailedToMapCommand {
    return new FailedToMapCommand(
      `No se pudo mapear el nombre de clase para el comando ${commandClassName}`
    );
  }

  /**
   * Crea una nueva instancia de FailedToMapCommand para el nombre de método de comando dado.
   *
   * @param commandClassName El nombre de la clase del comando.
   * @returns Una nueva instancia de FailedToMapCommand.
   */
  static methodName(commandClassName: string): FailedToMapCommand {
    return new FailedToMapCommand(
      `No se pudo mapear el nombre de método para el comando ${commandClassName}`
    );
  }
}
