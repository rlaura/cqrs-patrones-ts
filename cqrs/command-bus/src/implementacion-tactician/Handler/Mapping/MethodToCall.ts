import { MethodDoesNotExist } from "./MethodDoesNotExist";

/**
 * Representa el método que se debe llamar en el controlador.
 */
export class MethodToCall {
  private className: string;
  private methodName: string;

  /**
   * Crea una nueva instancia de MethodToCall.
   * 
   * @param className El nombre de la clase del controlador.
   * @param methodName El nombre del método del controlador.
   * @throws MethodDoesNotExist Si el método especificado no existe en la clase del controlador.
   */
  constructor(className: string, methodName: string) {
      // Si el método realmente no existe, también comprobaremos si __call existe (principalmente para
      // fines de compatibilidad). Dicho esto, no cambiaremos el nombre del método a __call porque nuestro
      // analizador de análisis estático aún puede inferir datos del nombre original del método.
      if (!this.methodExists(className, methodName) && !this.methodExists(className, '__call')) {
          throw new MethodDoesNotExist(className, methodName);
      }

      this.className = className;
      this.methodName = methodName;
  }

  /**
   * Devuelve el nombre de la clase del controlador.
   * 
   * @returns El nombre de la clase del controlador.
   */
  getClassName(): string {
      return this.className;
  }

  /**
   * Devuelve el nombre del método del controlador.
   * 
   * @returns El nombre del método del controlador.
   */
  getMethodName(): string {
      return this.methodName;
  }

  /**
   * Comprueba si el método especificado existe en la clase.
   * 
   * @param className El nombre de la clase.
   * @param methodName El nombre del método.
   * @returns True si el método existe, False en caso contrario.
   */
  private methodExists(className: string, methodName: string): boolean {
      // Aquí implementarías la lógica para comprobar si el método realmente existe en la clase.
      // Esto podría variar dependiendo de tu entorno y de cómo estés gestionando las clases en TypeScript.
      // Por ejemplo, podrías usar el método `hasOwnProperty` para verificar si el método existe en la clase.
      // Este método podría requerir ajustes dependiendo de cómo estés organizando tus clases en TypeScript.
      // Aquí se proporciona solo un ejemplo básico.
      // Ejemplo:
      // return Object.prototype.hasOwnProperty.call(className.prototype, methodName);
      return true; // Devuelve true como ejemplo básico.
  }
}