import { CommandToHandlerMapping } from "../CommandToHandlerMapping";
import { FailedToMapCommand } from "../FailedToMapCommand";
import { MethodToCall } from "../MethodToCall";

/**
 * Este mapeo es útil cuando estás trabajando con una lista compilada o una aplicación heredada
 * sin una convención de nomenclatura consistente.
 *
 * La matriz de mapeo debe estar en el siguiente formato:
 *
 *      [
 *          SomeCommand::class => [SomeHandler::class, 'handle'],
 *          OtherCommand::class => [WhateverHandler::class, 'handleOtherCommand'],
 *          ...
 *      ]
 */
export class MapByStaticList implements CommandToHandlerMapping {
  /** @var { [key: string]: [string, string] } */
  private mapping: { [key: string]: [string, string] };

  /**
   * Crea una nueva instancia de MapByStaticList.
   * 
   * @param mapping El mapeo estático de comandos a manejadores.
   */
  constructor(mapping: { [key: string]: [string, string] }) {
      this.mapping = mapping;
  }

  /**
   * Encuentra el manejador para el comando dado.
   * 
   * @param commandFQCN El FQCN (Fully Qualified Class Name) del comando.
   * @throws FailedToMapCommand Si no se puede mapear el comando a un manejador.
   * @returns Un objeto que describe el método a llamar en el controlador.
   */
  findHandlerForCommand(commandFQCN: string): MethodToCall {
      if (!this.mapping.hasOwnProperty(commandFQCN)) {
          throw FailedToMapCommand.className(commandFQCN);
      }

      return new MethodToCall(
          this.mapping[commandFQCN][0],
          this.mapping[commandFQCN][1]
      );
  }
}