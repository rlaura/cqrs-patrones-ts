/**
 * Excepción lanzada cuando un objeto manejador específico no puede ser usado en un objeto de comando.
 *
 * La razón más común es que el método receptor está ausente o tiene un nombre incorrecto.
 */
export class MethodDoesNotExist extends Error {
  private className: string;
  private methodName: string;

  /**
   * Crea una nueva instancia de MethodDoesNotExist.
   *
   * @param message El mensaje de error.
   * @param className El nombre de la clase del controlador.
   * @param methodName El nombre del método del controlador.
   */
  constructor(className: string, methodName: string) {
    super(`El método del controlador ${className}.${methodName}. no existe. Por favor, consulte a su estratega.
    configuración de mapeo o verifique verificar que' ${methodName} en realidad se declara en ${methodName},`);
    this.className = className;
    this.methodName = methodName;
  }

  /**
   * Devuelve el nombre de la clase del controlador.
   *
   * @returns El nombre de la clase del controlador.
   */
  getClassName(): string {
    return this.className;
  }

  /**
   * Devuelve el nombre del método del controlador.
   *
   * @returns El nombre del método del controlador.
   */
  getMethodName(): string {
    return this.methodName;
  }

  /**
   * Crea una nueva instancia de MethodDoesNotExist.
   *
   * @param className El nombre de la clase del controlador.
   * @param methodName El nombre del método del controlador.
   * @returns Una nueva instancia de MethodDoesNotExist.
   */
  static on(className: string, methodName: string): MethodDoesNotExist {
    const message = `El método de manejo ${className}::${methodName} no existe. Por favor, verifica tu configuración de mapeo de Tactician o verifica que ${methodName} esté declarado en ${className}.`;
    return new MethodDoesNotExist(className, methodName);
  }
}
