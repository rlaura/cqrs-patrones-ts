import { MethodToCall } from "./MethodToCall";

/**
 * Dado el nombre de una clase de comando, encuentra el método del controlador al que
 * debemos pasar el comando real.
 */
export interface CommandToHandlerMapping {
  /**
   * Encuentra el manejador para el comando dado.
   *
   * @param commandFQCN El FQCN (Fully Qualified Class Name) del comando.
   * @throws FailedToMapCommand Si no se puede mapear el comando a un manejador.
   * @returns Un objeto que describe el método a llamar en el controlador.
   */
  findHandlerForCommand(commandFQCN: string): MethodToCall;
}
