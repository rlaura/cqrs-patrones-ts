import { Middleware } from "../Middleware";

/**
 * Nuestro middleware básico para ejecutar comandos.
 *
 * ¡Siéntete libre de usar esto como punto de partida para tu propio middleware! :)
 */
export class CommandHandlerMiddleware implements Middleware {
  private container: ContainerInterface;
  private mapping: CommandToHandlerMapping;

  /**
   * Crea una nueva instancia de CommandHandlerMiddleware.
   * 
   * @param container El contenedor de dependencias PSR-11.
   * @param mapping El mapeo de comandos a manejadores.
   */
  constructor(container: ContainerInterface, mapping: CommandToHandlerMapping) {
      this.container = container;
      this.mapping = mapping;
  }

  /**
   * Ejecuta un comando y opcionalmente devuelve un valor.
   * 
   * @param command El comando a ejecutar.
   * @param next La función de siguiente middleware.
   * @returns El resultado de ejecutar el comando.
   */
  execute(command: object, next: () => any): any {
      // 1. Basándonos en el comando que recibimos, obtenemos el método Handler a llamar.
      const methodToCall = this.mapping.findHandlerForCommand(command.constructor.name);

      // 2. Obtenemos una instancia del Handler desde nuestro contenedor PSR-11.
      //    Esto asume que el identificador del contenedor es el mismo que el nombre de la clase, pero
      //    ¡puedes escribir tu propio middleware para cambiar esta suposición! :)
      const handler = this.container.get(methodToCall.getClassName());

      // 3. Invocamos el método correcto en el manejador y pasamos el comando.
      return handler[methodToCall.getMethodName()](command);
  }
}