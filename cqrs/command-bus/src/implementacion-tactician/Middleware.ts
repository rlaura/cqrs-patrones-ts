/**
 * Los middlewares son los plugins de Tactician. Reciben cada comando que se
 * pasa al CommandBus y pueden tomar cualquier acción que elijan. Los middlewares
 * pueden continuar el procesamiento del comando pasando el comando que reciben al
 * callable $next, que es esencialmente el "siguiente" middleware en la cadena.
 *
 * Dependiendo de dónde invoquen el callable $next, los middlewares pueden ejecutar
 * su lógica personalizada antes o después de que se maneje el comando. También pueden
 * modificar, registrar o reemplazar el comando que reciben. El cielo es el límite.
 */
export interface Middleware {
  /**
   * Ejecuta el middleware para el comando dado.
   *
   * @param command El comando a procesar.
   * @param next La función de siguiente middleware.
   * @returns El resultado del middleware.
   */
  execute(command: object, next: () => any): any;
}
