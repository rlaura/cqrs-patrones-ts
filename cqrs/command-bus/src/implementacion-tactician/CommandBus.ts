import { Middleware } from "./Middleware";

/**
 * Recibe un comando y lo envía a través de una cadena de middlewares para su procesamiento.
 */
export class CommandBus {
  /** @var {(command: object) => any} */
  private middlewareChain: (command: object) => any;

  /**
   * Crea una nueva instancia de CommandBus.
   * 
   * @param middleware La lista de middlewares a aplicar.
   */
  constructor(...middleware: Middleware[]) {
      this.middlewareChain = this.createExecutionChain(middleware);
  }

  /**
   * Ejecuta el comando dado y opcionalmente devuelve un valor.
   * 
   * @param command El comando a manejar.
   * @returns El resultado de manejar el comando.
   */
  handle(command: object): any {
      return this.middlewareChain(command);
  }

  /**
   * Crea una cadena de ejecución para los middlewares dados.
   * 
   * @param middlewareList La lista de middlewares.
   * @returns Una función que representa la cadena de ejecución de los middlewares.
   */
  private createExecutionChain(middlewareList: Middleware[]): (command: object) => any {
      let lastCallable: (command: object) => any = () => null;

      while (middlewareList.length > 0) {
          const middleware = middlewareList.pop();
          if (middleware) {
              lastCallable = (command: object) => middleware.execute(command, lastCallable);
          }
      }

      return lastCallable;
  }
}