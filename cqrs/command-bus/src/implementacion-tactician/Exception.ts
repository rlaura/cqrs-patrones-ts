/**
 * Marca de interfaz para todas las excepciones de Tactician
 */
interface TacticianException extends Error {}

// Ejemplo de excepción personalizada que implementa TacticianException
export class CustomException implements TacticianException {
  constructor(public message: string) {
  }
}
