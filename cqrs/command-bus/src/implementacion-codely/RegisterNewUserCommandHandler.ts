import { ICommand } from "./interfaces/ICommand";
import { ICommandHandler } from "./interfaces/ICommandHandler";
import { RegisterNewUserCommand } from "./RegisterNewUserCommand";

export class RegisterNewUserCommandHandler
  implements ICommandHandler<RegisterNewUserCommand>
{
  // constructor(private _registerNewUserCreator: any) {}
  /**
   * esto permite que el command se agrege al bus de comandos de cierto modo 
   * en la clase CommandHandlers
   */
  subscribedTo(): ICommand {
    console.log("suscribiendo el comando de ---- RegisterNewUserCommand");
    return RegisterNewUserCommand;
  }

  async handle(command: RegisterNewUserCommand): Promise<void> {
    const id = command.id;
    const emailAddress = command.emailAddress;
    const userName = command.userName;
    const password = command.password;

    console.log("CREANDO UN NUEVO REGISTRO DE USUARIO");
    console.log("ID", id);
    console.log("EMAIL", emailAddress);
    console.log("USERNAME", userName);
    console.log("PASSWORD", password);
    // await this._registerNewUserCreator.run({id,emailAddress,userName,password});
  }
}
