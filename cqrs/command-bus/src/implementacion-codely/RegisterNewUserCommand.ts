import { ICommand } from "./interfaces/ICommand";

export class RegisterNewUserCommand implements ICommand {
  private _id: string;
  private _emailAddress: string;
  private _userName: string;
  private _password: string;

  constructor(
    id: string,
    emailAddress: string,
    userName: string,
    password: string
  ) {
    this._id = id;
    this._emailAddress = emailAddress;
    this._userName = userName;
    this._password = password;
  }

  get id(): string {
    return this._id;
  }

  get emailAddress(): string {
    return this._emailAddress;
  }

  get userName(): string {
    return this._userName;
  }

  get password(): string {
    return this._password;
  }
}
