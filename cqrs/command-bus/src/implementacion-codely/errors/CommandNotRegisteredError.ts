import { ICommand } from "../interfaces/ICommand";

/**
 * clase CommandNotRegisteredError que extiende la clase Error.
 */
export class CommandNotRegisteredError extends Error {
  /**
   * Define un constructor que toma un parámetro de tipo Command.
   * Llama al constructor de la clase base (Error) con un mensaje que indica que el comando no tiene un manejador asociado.
   * @param command acepta un comando
   */
  constructor(command: ICommand) {
    /**
     * Llama al constructor de la clase base (Error) con un mensaje que indica que el comando no tiene un manejador asociado.
     */
    super(
      `El comando <${command.constructor.name}> no tiene un controlador de comandos asociado`
    );
    // super(`The command <${command.constructor.name}> hasn't a command handler associated`);
  }
}
