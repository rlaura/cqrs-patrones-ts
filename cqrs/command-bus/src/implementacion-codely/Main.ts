import { CommandHandlers } from "./command/CommandHandlers";
import { InMemoryCommandBus } from "./command/InMemoryCommandBus";
import { RegisterNewUserCommand } from "./RegisterNewUserCommand";
import { RegisterNewUserCommandHandler } from "./RegisterNewUserCommandHandler";

async function main() {
  const commandBus = new InMemoryCommandBus(
    new CommandHandlers([new RegisterNewUserCommandHandler()])
  );

  try {
    // Creamos un comando para crear un usuario
    const createUserCommand = new RegisterNewUserCommand(
      "sdf23442342-sdfas1231-sdadqw134-adsad",
      "alguien@example.com",
      "john tittor",
      "123456"
    );

    // Ejecutamos el comando
    await commandBus.dispatch(createUserCommand);
    
  } catch (error: any) {
    console.error("Error:", error);
  }
}

main();
