import { ICommand } from './ICommand';
/**
 * Define una interfaz llamada CommandBus que especifica un contrato para un bus de comandos.
 */
export interface ICommandBus {

  /**
   * @param command acepta un comando y devuelve una promesa sin valor.
   */
  dispatch(command: ICommand): Promise<void>;
}
