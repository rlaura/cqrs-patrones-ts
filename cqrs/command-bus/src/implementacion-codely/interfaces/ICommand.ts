/**
 * Command Bus (Bus de Comandos)
 * Función: Enruta comandos a sus manejadores específicos para modificar el estado del sistema.
 * Ejemplos de comandos: Crear un usuario, actualizar un producto, eliminar una orden.
 * Momento de uso: Cuando se desea realizar una acción que modifica el estado del sistema.
 *                 por ejemplo un create, update, delete
 */

/**
 * Interfas que se utiliza para dar una especie de tipado
 */
export interface ICommand {}
