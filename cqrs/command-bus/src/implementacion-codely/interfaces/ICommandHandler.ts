import { ICommand } from './ICommand';
/**
 * Define una interfaz llamada ICommandHandler que toma un tipo genérico T que debe ser una subclase de ICommand.
 * <T extends ICommand>  T es un tipo genérico que debe ser una subclase o implementación de la interfaz ICommand. 
 *                      Esto significa que T debe ser un tipo específico de comando.
 */
export interface ICommandHandler<T extends ICommand> {
  /**
   * devuelve un objeto de tipo Command.
   */
  subscribedTo(): ICommand;
  /**
   * Devuelve una promesa que eventualmente resuelve en void
   * el manejo del comando puede implicar operaciones asíncronas.
   * @param command toma un comando de tipo T (que es una subclase de ICommand) y lo maneja
   */
  handle(command: T): Promise<void>;
}
