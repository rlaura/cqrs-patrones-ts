import { CommandNotRegisteredError } from "../errors/CommandNotRegisteredError";
import { ICommand } from "../interfaces/ICommand";
import { ICommandHandler } from "../interfaces/ICommandHandler";

/**
 * Representa un mapa de controladores de comandos.
 */
export class CommandHandlers extends Map<ICommand, ICommandHandler<ICommand>> {
  /**
   * Crea una nueva instancia de CommandHandlers.
   * @param {Array<CommandHandler<Command>>} commandHandlers - Los controladores de comandos para inicializar el mapa.
   * @return {CommandHandlers} Una nueva instancia de CommandHandlers.
   */
  constructor(commandHandlers: Array<ICommandHandler<ICommand>>) {
    super();
    /**
     * Itera sobre cada elemento del array commandHandlers utilizando el método forEach
     */
    commandHandlers.forEach((commandHandler) => {
      this.set(commandHandler.subscribedTo(), commandHandler);
    });
  }

  /**
   * Obtiene el controlador de comando asociado a un comando.
   * @param {Command} command - El comando para el que se busca el controlador.
   * @return {CommandHandler<Command>} El controlador de comando asociado al comando.
   * @throws {CommandNotRegisteredError} Se lanza si no se encuentra ningún controlador de comando asociado al comando.
   */
  public get(command: ICommand): ICommandHandler<ICommand> {
    const commandHandler = super.get(command.constructor);

    if (!commandHandler) {
      throw new CommandNotRegisteredError(command);
    }

    return commandHandler;
  }
}
