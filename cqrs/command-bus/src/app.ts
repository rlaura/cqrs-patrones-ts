// https://www.npmjs.com/package/cqrs?activeTab=code
// https://www.npmjs.com/package/@nestjs/cqrs

// https://www.npmjs.com/package/@figedi/cqrs?activeTab=code
// https://www.npmjs.com/package/@tomasjs/cqrs?activeTab=readme
// https://github.com/erickjth/simple-command-bus/blob/master/src/handler/CommandHandlerMiddleware.js

/**
 * tactician
 * command bus
 * https://github.com/thephpleague/tactician/blob/master/src/Handler/Mapping/MapByNamingConvention/ClassName/ClassNameInflector.php
 * 
 * cqrs
 * https://github.com/broadway/broadway/blob/master/src/Broadway/EventStore/EventStore.php
 * https://github.com/broadway/broadway-bundle?tab=readme-ov-file
 * 
 * service bus
 * https://github.com/spiral-packages/cqrs
 */

// export * from "./implementacion-01/Main";
// export * from "./implementacion-02/Main";
// export * from "./implementacion-03/Main";
// export * from "./implementacion-04/Main";
export * from "./implementacion-asincrona/Main";
// export * from "./implementacion-codely/Main";
// export * from "./implementacion-tactician/Main";
