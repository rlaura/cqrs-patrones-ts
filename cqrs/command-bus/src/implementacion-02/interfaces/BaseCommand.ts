import { ICommand } from "./ICommand";

/**
 * Clase abstracta que sirve como base para implementar comandos.
 */
export abstract class BaseCommand implements ICommand {
  /**
   * Método abstracto que debe ser implementado por las clases derivadas para ejecutar el comando.
   * @returns Una promesa que se resuelve cuando el comando se ha ejecutado con éxito.
   */
  abstract execute(): Promise<void>;
}