
import { CommandHandler } from "./decorator/CommandHandler";
import { BaseCommand } from "./interfaces/BaseCommand";

// Ejemplo de un comando concreto
@CommandHandler("CreateUserCommand")
export class CreateUserCommand extends BaseCommand {
  constructor(private _username: string, private _email: string) {
    super();
  }

  get username() {
    return this._username;
  }

  async execute(): Promise<void> {
    // Lógica para crear un usuario en la base de datos
    console.log(
      `Creando usuario con nombre de usuario '${this._username}' y correo electrónico '${this._email}'...`
    );
  }
}
