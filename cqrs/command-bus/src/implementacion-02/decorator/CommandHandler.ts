/**
 * Decorador que asigna un nombre de comando a una clase manejadora de comando.
 * @param commandName El nombre del comando que se asignará a la clase manejadora.
 * @returns Un decorador de clase.
 */
export function CommandHandler(commandName: string) {
  return function <T extends { new (...args: any[]): {} }>(constructor: T) {
    constructor.prototype.commandName = commandName;
  };
}
