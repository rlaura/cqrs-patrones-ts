import { CreateUserCommand } from "./CreateUserCommand";
import { BaseCommandHandler } from "./interfaces/BaseCommandHandler";

// Ejemplo de un handler de comando concreto
export class CreateUserCommandHandler extends BaseCommandHandler<CreateUserCommand> {
  async handle(command: CreateUserCommand): Promise<void> {
    // Lógica para manejar la creación de usuario
    // Aquí podrías incluir la lógica de la transacción
    await command.execute();
    console.log(`El usuario '${command.username}' se creó correctamente.`);
  }
}