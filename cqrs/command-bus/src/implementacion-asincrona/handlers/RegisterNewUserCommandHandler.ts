import { RegisterNewUserCommand } from "../commands/RegisterNewUserCommand";
import { ICommandHandler } from "../interfaces/ICommandHandler";

export class RegisterNewUserCommandHandler
  implements ICommandHandler<RegisterNewUserCommand>
{
  // constructor(private _registerNewUserCreator: any) {}

  async handle(command: RegisterNewUserCommand): Promise<void> {
    const id = command.id;
    const emailAddress = command.emailAddress;
    const userName = command.userName;
    const password = command.password;

    console.log("CREANDO UN NUEVO REGISTRO DE USUARIO");
    console.log("ID", id);
    console.log("EMAIL", emailAddress);
    console.log("USERNAME", userName);
    console.log("PASSWORD", password);

    // Simular una operación asincrónica
    await new Promise(resolve => setTimeout(resolve, 1000));
    console.log(`======================================================`);
    console.log(`Procesando RegisterNewUserCommand con carga útil: ${JSON.stringify(command.payload)}`);
    // await this._registerNewUserCreator.run({id,emailAddress,userName,password});
  }
}
