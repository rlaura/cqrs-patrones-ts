import { ICommand } from "../interfaces/ICommand";

export class RegisterNewUserCommand implements ICommand {
  public name = "RegisterNewUserCommand";
  public payload: any;

  private _id: string;
  private _emailAddress: string;
  private _userName: string;
  private _password: string;

  constructor(payload: {
    id: string;
    emailAddress: string;
    userName: string;
    password: string;
  }) {
    this.payload = payload;
    this._id = payload.id;
    this._emailAddress = payload.emailAddress;
    this._userName = payload.userName;
    this._password = payload.password;
  }

  get id(): string {
    return this._id;
  }

  get emailAddress(): string {
    return this._emailAddress;
  }

  get userName(): string {
    return this._userName;
  }

  get password(): string {
    return this._password;
  }
}
