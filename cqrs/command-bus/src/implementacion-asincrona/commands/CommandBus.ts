import { ICommand } from "../interfaces/ICommand";
import { ICommandHandler } from "../interfaces/ICommandHandler";
import { Middleware } from "../interfaces/Middleware";

export class CommandBus {
  private handlers = new Map<string, ICommandHandler<ICommand>>();
  private middlewares: Middleware[] = [];

  public registerHandler<T extends ICommand>(commandName: string, handler: ICommandHandler<T>): void {
    this.handlers.set(commandName, handler);
  }

  public addMiddleware(middleware: Middleware): void {
    this.middlewares.push(middleware);
  }

  public async dispatch(command: ICommand): Promise<void> {
    const handler = this.handlers.get(command.name);

    if (!handler) {
      throw new Error(`No se encontró ningún controlador para el comando: ${command.name}`);
    }

    const executeMiddlewares = async (index: number): Promise<void> => {
      if (index < this.middlewares.length) {
        const middleware = this.middlewares[index];
        await middleware(command, () => executeMiddlewares(index + 1));
      } else {
        await handler.handle(command);
      }
    };

    await executeMiddlewares(0);
  }
}