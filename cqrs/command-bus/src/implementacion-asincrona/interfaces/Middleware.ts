import { ICommand } from "./ICommand";

export type Middleware = (
  command: ICommand,
  next: () => Promise<void>
) => Promise<void>;
