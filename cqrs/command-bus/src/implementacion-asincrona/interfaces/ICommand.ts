export interface ICommand {
  name: string;
  payload: any;
}
