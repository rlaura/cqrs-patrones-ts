import { Middleware } from "./Middleware";

export const LoggerMiddleware: Middleware = async (command, next) => {
  console.log(`Comando de manejo: ${command.name}`);
  await next();
  console.log(`Comando manejado: ${command.name}`);
};