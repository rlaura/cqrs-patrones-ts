import { CommandBus } from "./commands/CommandBus";
import { RegisterNewUserCommand } from "./commands/RegisterNewUserCommand";
import { RegisterNewUserCommandHandler } from "./handlers/RegisterNewUserCommandHandler";
import { LoggerMiddleware } from "./interfaces/LoggerMiddleware";

async function main() {
  const commandBus = new CommandBus();

  // Registrar el manejador
  commandBus.registerHandler(
    "RegisterNewUserCommand",
    new RegisterNewUserCommandHandler()
  );

  // Agregar middlewares
  commandBus.addMiddleware(LoggerMiddleware);

  // Crear y despachar un comando
  // Creamos un comando para crear un usuario
  const createUserCommand = new RegisterNewUserCommand({
    id: "sdf23442342-sdfas1231-sdadqw134-adsad",
    emailAddress: "alguien@example.com",
    userName: "john tittor",
    password: "123456",
  });
  
  commandBus.dispatch(createUserCommand).catch(console.error);
}

main();
