import { BaseCommand } from "../interfaces/BaseCommand";
import { BaseCommandHandler } from "../interfaces/BaseCommandHandler";

/**
 * Clase que representa un bus de comandos.
 */
export class CommandBus {
  private handlers: Map<string, BaseCommandHandler<BaseCommand>> = new Map();

  /**
   * Registra un manejador de comando en el bus de comandos.
   * @param handler El manejador de comando a registrar.
   */
  registerHandler(handler: BaseCommandHandler<BaseCommand>): void {
    const commandName = handler.commandName;
    if (commandName && !this.handlers.has(commandName)) {
      this.handlers.set(commandName, handler);
    }
  }

  /**
   * Ejecuta un comando utilizando el manejador correspondiente.
   * @param command El comando a ejecutar.
   * @throws Error si no hay ningún manejador registrado para el comando.
   */
  async executeCommand(command: BaseCommand): Promise<void> {
    const commandName = command.constructor.prototype.commandName;
    const handler = this.handlers.get(commandName);
    if (!handler) {
      throw new Error(`Ningún controlador registrado para el comando '${commandName}'.`);
    }
    await handler.handle(command);
  }
}
