/**
 * Interfaz que define un comando.
 */
export interface ICommand {
  /**
   * Ejecuta el comando.
   * @returns Una promesa que se resuelve cuando el comando se ha ejecutado con éxito.
   */
  execute(): Promise<void>;
}
