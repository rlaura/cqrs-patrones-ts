import { ICommand } from "./ICommand";
import { ICommandHandler } from "./ICommandHandler";

/**
 * Clase abstracta que sirve como base para implementar manejadores de comandos.
 * @template T - Tipo de comando que maneja el manejador.
 */
export abstract class BaseCommandHandler<T extends ICommand>
  implements ICommandHandler<T>
{
  /**
   * El nombre del comando que maneja el manejador.
   */
  abstract commandName: string;

  /**
   * Método abstracto que debe ser implementado por las clases derivadas para manejar el comando.
   * @param command El comando que se va a manejar.
   * @returns Una promesa que se resuelve cuando el comando se ha manejado con éxito.
   */
  abstract handle(command: T): Promise<void>;
}
