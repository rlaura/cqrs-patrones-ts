import { CreateUserCommand } from "./CreateUserCommand";
import { ICommandHandler } from "./interfaces/ICommandHandler";

// Handler de comando para el CreateUserCommand
export class CreateUserCommandHandler
  implements ICommandHandler<CreateUserCommand>
{
  async handle(command: CreateUserCommand): Promise<void> {
    // Lógica para manejar la creación de usuario
    // Aquí podrías incluir la lógica de la transacción
    await command.execute();
    console.log(`User '${command.username}' created successfully.`);
  }
}
