import { CommandBus } from "./command/CommandBus";
import { CreateUserCommand } from "./CreateUserCommand";
import { CreateUserCommandHandler } from "./CreateUserCommandHandler";

async function main() {
  // Creamos una instancia del command bus
  const commandBus = new CommandBus();

  // Registramos el handler para el comando de crear usuario
  commandBus.registerHandler(
    "CreateUserCommand",
    new CreateUserCommandHandler()
  );

  try {
    // Creamos un comando para crear un usuario
    const createUserCommand = new CreateUserCommand(
      "john_doe",
      "john@example.com"
    );

    // Ejecutamos el comando
    await commandBus.executeCommand(createUserCommand);
  } catch (error: any) {
    console.error("Error:", error.message);
  }
}

main();
