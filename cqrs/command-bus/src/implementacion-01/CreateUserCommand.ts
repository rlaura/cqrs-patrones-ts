import { ICommand } from "./interfaces/ICommand";


export class CreateUserCommand implements ICommand {
  constructor(private _username: string, private _email: string) {}

  get username() {
    return this._username;
  }

  async execute(): Promise<void> {
    console.log(
      `Creando usuario con nombre de usuario '${this._username}' y correo electrónico '${this._email}'...`
    );
  }
}
