import { ICommand } from "../interfaces/ICommand";
import { ICommandHandler } from "../interfaces/ICommandHandler";
/**
 * Clase que representa un bus de comandos.
 */
export class CommandBus {
  private handlers: Map<string, ICommandHandler<ICommand>>;

  /**
   * Crea una nueva instancia de CommandBus.
   */
  constructor() {
    this.handlers = new Map();
  }

  /**
   * Registra un manejador de comando para un comando específico.
   * @param commandName El nombre del comando.
   * @param handler El manejador de comando a registrar.
   * @throws Error si ya existe un manejador registrado para el comando.
   */
  public registerHandler<T extends ICommand>(
    commandName: string,
    handler: ICommandHandler<T>
  ): void {
    if (this.handlers.has(commandName)) {
      throw new Error(
        `El controlador del comando '${commandName}' ya está registrado.`
      );
    }
    this.handlers.set(commandName, handler);
  }

  /**
   * Ejecuta un comando utilizando el manejador registrado para dicho comando.
   * @param command El comando a ejecutar.
   * @returns Una promesa que se resuelve cuando el comando se ha ejecutado con éxito.
   * @throws Error si no hay ningún manejador registrado para el comando.
   */
  public async executeCommand(command: ICommand): Promise<void> {
    const commandName = command.constructor.name;
    const handler = this.handlers.get(commandName);

    if (!handler) {
      throw new Error(
        `No hay ningún controlador registrado para el comando '${commandName}'.`
      );
    }

    await handler.handle(command);
  }
}
