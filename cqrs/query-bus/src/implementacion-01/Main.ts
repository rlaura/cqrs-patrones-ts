import { GetUserInfoQuery } from "./GetUserInfoQuery";
import { GetUserInfoQueryHandler } from "./GetUserInfoQueryHandler";
import { QueryBus } from "./query/QueryBus";

async function main() {
  try {
    const queryBus = new QueryBus();
    const getUserInfoQueryHandler = new GetUserInfoQueryHandler();
    queryBus.registerHandler("GetUserInfoQuery", getUserInfoQueryHandler);

    const userId = "id_del_usuario";
    const userInfoQuery = new GetUserInfoQuery(userId);
    queryBus
      .executeQuery(userInfoQuery)
      .then((userInfo) => {
        console.log(userInfo);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
