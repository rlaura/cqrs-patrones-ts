import { IQuery } from "../interfaces/IQuery";
import { IQueryHandler } from "../interfaces/IQueryHandler";

/**
 * Clase para el bus de consultas.
 * Esta clase proporciona un mecanismo para registrar y ejecutar manejadores de consultas.
 */
export class QueryBus {
    private handlers: Map<string, IQueryHandler<any, any>>;
  
    /**
     * Crea una instancia del bus de consultas.
     */
    constructor() {
        this.handlers = new Map();
    }
  
    /**
     * Registra un manejador para un tipo de consulta específico.
     * @typeparam TQuery El tipo de la consulta que manejará el manejador.
     * @typeparam TResult El tipo del resultado que se espera de manejar la consulta.
     * @param queryType El nombre del tipo de consulta.
     * @param handler El manejador de consulta que se registrará.
     * @throws Error Si ya existe un manejador registrado para el tipo de consulta especificado.
     */
    registerHandler<TQuery extends IQuery<TResult>, TResult>(queryType: string, handler: IQueryHandler<TQuery, TResult>) {
        if (this.handlers.has(queryType)) {
            throw new Error(`El controlador para el tipo de consulta '${queryType}' ya está registrado.`);
        }
        this.handlers.set(queryType, handler);
    }
  
    /**
     * Ejecuta una consulta utilizando el manejador registrado correspondiente.
     * @typeparam TQuery El tipo de la consulta que se ejecutará.
     * @typeparam TResult El tipo del resultado que se espera de la consulta.
     * @param query La consulta que se va a ejecutar.
     * @returns Una promesa que se resuelve en el resultado de la consulta.
     * @throws Error Si no se encuentra ningún manejador registrado para el tipo de consulta especificado.
     */
    async executeQuery<TQuery extends IQuery<TResult>, TResult>(query: TQuery): Promise<TResult> {
        const queryType = (query.constructor as any).name;
        const handler = this.handlers.get(queryType);
        if (!handler) {
            throw new Error(`Ningún controlador registrado para el tipo de consulta '${queryType}'.`);
        }
        return handler.execute(query);
    }
  }