/**
 * Interfaz genérica para consultas.
 * Esta interfaz define una estructura genérica para representar consultas.
 * @typeparam TResponse El tipo de la respuesta que se espera de la consulta.
 */
export interface IQuery<TResponse> {}