import { IQuery } from "./interfaces/IQuery";

// Ejemplo de implementación de Query y QueryHandler
export interface UserInfo {
  userId: string;
  username: string;
  // Otros campos de información del usuario
}

export class GetUserInfoQuery implements IQuery<UserInfo> {
  constructor(public userId: string) {}
}
