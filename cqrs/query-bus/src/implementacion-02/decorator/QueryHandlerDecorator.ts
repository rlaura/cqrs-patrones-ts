import { IQueryHandler } from "../../implementacion-01/interfaces/IQueryHandler";
import { IQuery } from "../interfaces/IQuery";
import { QueryBus } from "../query/QueryBus";

// export function QueryHandlerDecorator<TQuery extends IQuery<TResult>, TResult>(queryType: string) {
//   return function <T extends { new(...args: any[]): {} }>(constructor: T) {
//       return class extends constructor {
//           constructor(...args: any[]) {
//               super(...args);
//               const queryBus = new QueryBus();
//               queryBus.registerHandler(queryType, constructor);
//           }
//       };
//   };
// }

// export function QueryHandlerDecorator<TQuery extends IQuery<TResult>, TResult>(queryType: string) {
//     return function <T extends new (...args: any[]) => IQueryHandler<TQuery, TResult>>(constructor: T) {
//         return class extends constructor {
//             constructor(...args: any[]) {
//                 super(...args);
//                 const queryBus = new QueryBus();
//                 queryBus.registerHandler(queryType, constructor); // Cambiar constructor por constructor()
//             }
//         };
//     };
// }

/**
 * Decorador para los manejadores de consultas.
 * Este decorador registra automáticamente un manejador de consultas en el bus de consultas correspondiente.
 * @typeparam TQuery El tipo de la consulta que manejará el manejador.
 * @typeparam TResult El tipo del resultado que se espera de manejar la consulta.
 * @param queryType El nombre del tipo de consulta.
 * @returns Una función de decorador que toma un constructor de manejador de consultas y lo modifica para registrar automáticamente el manejador en el bus de consultas.
 */
export function QueryHandlerDecorator<TQuery extends IQuery<TResult>, TResult>(queryType: string) {
    return function <T extends new (...args: any[]) => IQueryHandler<TQuery, TResult>>(constructor: T) {
        return class extends constructor {
            /**
             * Crea una nueva instancia del manejador de consultas decorado.
             * Registra automáticamente el manejador en el bus de consultas correspondiente.
             * @param args Argumentos para el constructor del manejador de consultas.
             */
            constructor(...args: any[]) {
                super(...args);
                const queryBus = new QueryBus();
                queryBus.registerHandler(queryType, constructor);
            }
        };
    };
}