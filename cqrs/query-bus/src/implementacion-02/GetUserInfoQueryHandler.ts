
import { QueryHandlerDecorator } from "./decorator/QueryHandlerDecorator";
import { GetUserInfoQuery, UserInfo } from "./GetUserInfoQuery";
import { IQueryHandler } from "./interfaces/IQueryHandler";

@QueryHandlerDecorator<GetUserInfoQuery, UserInfo>("GetUserInfoQuery")
export class GetUserInfoQueryHandler implements IQueryHandler<GetUserInfoQuery, UserInfo> {
    async execute(query: GetUserInfoQuery): Promise<UserInfo> {
        // Logic to fetch user information
        const userInfo: UserInfo = {
            userId: query.userId,
            username: "Sample User",
            // Other fields fetched from database or any other data source
        };
        return userInfo;
    }
}