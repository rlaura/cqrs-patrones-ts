import { CoursesResponse } from './CoursesResponse';
import { CoursesFinder } from './CoursesFinder';
import { SearchAllCoursesQuery } from './SearchAllCoursesQuery';
import { IQueryHandler } from './interfaces/IQueryHandler';
import { IQuery } from './interfaces/IQuery';

export class SearchAllCoursesQueryHandler implements IQueryHandler<SearchAllCoursesQuery, CoursesResponse> {
  // constructor(private coursesFinder: CoursesFinder) {}

  subscribedTo(): IQuery {
    return SearchAllCoursesQuery;
  }

  async handle(_query: SearchAllCoursesQuery): Promise<any> {
    const data = [
      {
        id: "asdasd123s-asdasdas12-asasdas-123sdasd",
        name: "curso de nodejs",
        duration: "50h",
      },
      {
        id: "asdasd123s-asdasdas12-asasdas-123sdasd",
        name: "curso de go con fiber",
        duration: "50h",
      },
    ];
    
    return data;
    // return this.coursesFinder.run();
  }
}
