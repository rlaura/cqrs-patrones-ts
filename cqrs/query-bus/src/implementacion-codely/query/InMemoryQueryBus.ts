import { IQuery } from "../interfaces/IQuery";
import { IQueryBus } from "../interfaces/IQueryBus";
import { IResponse } from "../interfaces/IResponse";
import { QueryHandlers } from "./QueryHandlers";

/**
 * Implementación de un bus de consultas en memoria.
 */
export class InMemoryQueryBus implements IQueryBus {
  /**
   * Crea una nueva instancia de InMemoryQueryBus.
   * @param {QueryHandlers} queryHandlersInformation - La información de los controladores de consultas asociados al bus.
   * @return {InMemoryQueryBus} Una nueva instancia de InMemoryQueryBus.
   */
  constructor(private queryHandlersInformation: QueryHandlers) {}
  /**
   * Envía una consulta al bus y obtiene una respuesta.
   * @param {Query} query - La consulta a enviar al bus.
   * @return {Promise<R>} Una promesa que se resuelve con la respuesta a la consulta.
   */
  async ask<R extends IResponse>(query: IQuery): Promise<R> {
    const handler = this.queryHandlersInformation.get(query);

    return (await handler.handle(query)) as Promise<R>;
  }
}
