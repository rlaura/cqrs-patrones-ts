/**
 * Query Bus (Bus de Consultas)
 * Función: Enruta consultas a sus manejadores específicos para recuperar datos del sistema sin modificar su estado.
 * Ejemplos de consultas: Obtener un usuario por ID, buscar productos por categoría, calcular el total de una orden.
 * Momento de uso: Cuando se necesita información del sistema sin modificarlo.
 *                 cuando haces un read, findById, findAll, etc
 */

/**
 * se utiliza para simular el tipo query
 */
export interface IQuery {}
