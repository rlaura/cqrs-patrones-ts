import { IQuery } from './IQuery';
import { IResponse } from './IResponse';
/**
 * 
 */
export interface IQueryBus {
  /**
   * devuelve una promesa que eventualmente se resuelve en un tipo genérico R
   * R, es una subclase o implementación de la interfaz Response.
   * <R extends IResponse> R que extiende (o es subtipo de) la clase o interfaz Response
   * ask --> hacer
   * @param query toma un parámetro de tipo IQuery
   */
  ask<R extends IResponse>(query: IQuery): Promise<R>;
}
