// import { CourseRepository } from '../../domain/CourseRepository';
import { CoursesResponse } from "./CoursesResponse";

export class CoursesFinder {
  // constructor(private coursesRepository: CourseRepository) {}

  async run() {
    // const courses = await this.coursesRepository.searchAll();
    const courses = [
      {
        id: "asdasd123s-asdasdas12-asasdas-123sdasd",
        name: "curso de nodejs",
        duration: "50h",
      },
      {
        id: "asdasd123s-asdasdas12-asasdas-123sdasd",
        name: "curso de go con fiber",
        duration: "50h",
      },
    ];
    return new CoursesResponse(courses);
  }
}
