import { CoursesResponse } from "./CoursesResponse";
import { InMemoryQueryBus } from "./query/InMemoryQueryBus";
import { QueryHandlers } from "./query/QueryHandlers";
import { SearchAllCoursesQuery } from "./SearchAllCoursesQuery";
import { SearchAllCoursesQueryHandler } from "./SearchAllCoursesQueryHandler";

async function main() {
  const queryBus = new InMemoryQueryBus(
    new QueryHandlers([new SearchAllCoursesQueryHandler()])
  );

  try {
    const query = new SearchAllCoursesQuery();
    const data = await queryBus.ask<CoursesResponse>(query);
    console.log(data);
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
