import { GetUserInfoQuery } from "./GetUserInfoQuery";
import { GetUserInfoQueryHandler } from "./GetUserInfoQueryHandler";
import { QueryBus } from "./query/QueryBus";

async function main() {
  try {
    const queryBus = new QueryBus();
    queryBus.registerHandler("GetUserInfoQuery", GetUserInfoQueryHandler);

    const userId = "user_id";
    const userInfoQuery = new GetUserInfoQuery(userId);
    queryBus
      .executeQuery(userInfoQuery)
      .then((userInfo) => {
        console.log(userInfo);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
