import { GetUserInfoQuery, UserInfo } from "./GetUserInfoQuery";
import { IQueryHandler } from "./interfaces/IQueryHandler";

/**
 * Clase para el manejador de la consulta de obtener información del usuario.
 * Esta clase implementa la interfaz IQueryHandler para manejar la consulta GetUserInfoQuery y devolver información del usuario.
 */
export class GetUserInfoQueryHandler
  implements IQueryHandler<GetUserInfoQuery, UserInfo>
{
  /**
   * Ejecuta la consulta para obtener información del usuario.
   * @param query La consulta para obtener información del usuario.
   * @returns Una promesa que se resuelve en la información del usuario.
   */
  async execute(query: GetUserInfoQuery): Promise<UserInfo> {
    // Lógica para obtener la información del usuario
    const userInfo: UserInfo = {
      userId: query.userId,
      username: "Sample User",
      // Otros campos obtenidos de la base de datos u otra fuente de datos
    };
    return userInfo;
  }
}
