import { IQuery } from "./IQuery";

/**
 * Interfaz para el manejo de consultas.
 * Esta interfaz define un método para manejar consultas de un tipo específico y devolver un resultado.
 * @typeparam TQuery El tipo de la consulta que se manejará.
 * @typeparam TResult El tipo del resultado que se espera de manejar la consulta.
 */
export interface IQueryHandler<TQuery extends IQuery<TResult>, TResult> {
  /**
   * Ejecuta la consulta especificada y devuelve un resultado.
   * @param query La consulta que se va a manejar.
   * @returns Una promesa que se resuelve en el resultado de la consulta.
   */
  execute(query: TQuery): Promise<TResult>;
}
