import { IQuery } from "./interfaces/IQuery";

// Ejemplo de implementación de Query y QueryHandler
export interface UserInfo {
  userId: string;
  username: string;
  // Otros campos de información del usuario
}

/**
 * Clase para la consulta de obtener información del usuario.
 * Esta clase representa una consulta para obtener información de un usuario por su ID.
 * Implementa la interfaz genérica IQuery para especificar el tipo de respuesta esperada.
 */
export class GetUserInfoQuery implements IQuery<UserInfo> {
  /**
   * Crea una nueva instancia de la consulta para obtener información del usuario.
   * @param userId El ID del usuario del que se desea obtener la información.
   */
  constructor(public userId: string) {}
}
