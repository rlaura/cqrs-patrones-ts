export interface IEventHandler<T> {
  handle(event: T): void | Promise<void>;
}