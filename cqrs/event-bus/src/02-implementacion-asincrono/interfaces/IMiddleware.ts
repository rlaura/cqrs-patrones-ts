export interface IMiddleware<T> {
  (event: T, next: () => Promise<void> | void): Promise<void> | void;
}