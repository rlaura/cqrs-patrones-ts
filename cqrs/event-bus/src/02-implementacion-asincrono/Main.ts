import { EventBus } from "./events/EventBus";
import { OrderPlacedEvent } from "./events/OrderPlacedEvent";
import { UserCreatedEvent } from "./events/UserCreatedEvent";
import { OrderPlacedEventHandler } from "./handlers/OrderPlacedEventHandler";
import { UserCreatedEventHandler } from "./handlers/UserCreatedEventHandler";

/**
 * implementacion de un event bus sincrono
 */
async function main() {
  const eventBus = new EventBus();

  // Registro de manejadores de eventos
  const userCreatedHandler = new UserCreatedEventHandler();
  const unsubscribeUserCreated = eventBus.register(
    "UserCreatedEvent",
    userCreatedHandler
  );

  const orderPlacedHandler = new OrderPlacedEventHandler();
  const unsubscribeOrderPlaced = eventBus.register(
    "OrderPlacedEvent",
    orderPlacedHandler
  );

  // Registro de middlewares (opcional)
  eventBus.use("UserCreatedEvent", async (event, next) => {
    console.log(`Middleware: antes de manipular UserCreatedEvent`);
    await next();
    console.log(`Middleware: después de manejar UserCreatedEvent`);
  });

  eventBus.use("OrderPlacedEvent", async (event, next) => {
    console.log(`Middleware: antes de manipular OrderPlacedEvent`);
    await next();
    console.log(`Middleware: después de manejar OrderPlacedEvent`);
  });

  // Dispatching events
  const userCreatedEvent = new UserCreatedEvent("123", "Jose");
  eventBus.dispatch("UserCreatedEvent", userCreatedEvent);

  const orderPlacedEvent = new OrderPlacedEvent("456", "123");
  eventBus.dispatch("OrderPlacedEvent", orderPlacedEvent);

  // Desuscripción de manejadores
  unsubscribeUserCreated();
  unsubscribeOrderPlaced();
}

main();
