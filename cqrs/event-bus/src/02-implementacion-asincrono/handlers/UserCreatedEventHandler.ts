import { UserCreatedEvent } from "../events/UserCreatedEvent";
import { IEventHandler } from "../interfaces/IEventHandler";

export class UserCreatedEventHandler implements IEventHandler<UserCreatedEvent> {
  async handle(event: UserCreatedEvent): Promise<void> {
      console.log(`Usuario creado con ID: ${event.userId} y Nombre: ${event.name}`);
      // Lógica adicional aquí
  }
}