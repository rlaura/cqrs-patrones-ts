import { OrderPlacedEvent } from "../events/OrderPlacedEvent";
import { IEventHandler } from "../interfaces/IEventHandler";

export class OrderPlacedEventHandler implements IEventHandler<OrderPlacedEvent> {
  public async handle(event: OrderPlacedEvent): Promise<void> {
      console.log(`Pedido realizado con ID: ${event.orderId} para ID de usuario: ${event.userId}`);
      // Lógica adicional aquí
  }
}