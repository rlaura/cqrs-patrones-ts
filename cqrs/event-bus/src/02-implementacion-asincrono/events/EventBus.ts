import { IEventHandler } from "../interfaces/IEventHandler";
import { IMiddleware } from "../interfaces/IMiddleware";


export class EventBus {
    private handlers: { [eventName: string]: IEventHandler<any>[] } = {};
    private middlewares: { [eventName: string]: IMiddleware<any>[] } = {};
  
    public register<T>(eventName: string, handler: IEventHandler<T>): () => void {
        if (!this.handlers[eventName]) {
            this.handlers[eventName] = [];
        }
        this.handlers[eventName].push(handler);
  
        // Return an unsubscribe function
        return () => {
            this.handlers[eventName] = this.handlers[eventName].filter(h => h !== handler);
        };
    }
  
    public use<T>(eventName: string, middleware: IMiddleware<T>): void {
        if (!this.middlewares[eventName]) {
            this.middlewares[eventName] = [];
        }
        this.middlewares[eventName].push(middleware);
    }
  
    public async dispatch<T>(eventName: string, event: T): Promise<void> {
        const middlewares = this.middlewares[eventName] || [];
        const handlers = this.handlers[eventName] || [];
  
        const executeMiddlewares = (index: number): Promise<void> | void => {
            if (index < middlewares.length) {
                const middleware = middlewares[index];
                return middleware(event, () => executeMiddlewares(index + 1));
            } else {
                return executeHandlers();
            }
        };
  
        const executeHandlers = async (): Promise<void> => {
            for (const handler of handlers) {
                await handler.handle(event);
            }
        };
  
        await executeMiddlewares(0);
    }
  }

  // export class EventBus {
//   private handlers: { [eventName: string]: IEventHandler<any>[] } = {};
//   private middlewares: { [eventName: string]: IMiddleware<any>[] } = {};

//   public register<T>(eventName: string, handler: IEventHandler<T>): () => void {
//       if (!this.handlers[eventName]) {
//           this.handlers[eventName] = [];
//       }
//       this.handlers[eventName].push(handler);

//       // Return an unsubscribe function
//       return () => {
//           this.handlers[eventName] = this.handlers[eventName].filter(h => h !== handler);
//       };
//   }

//   public use<T>(eventName: string, middleware: IMiddleware<T>): void {
//       if (!this.middlewares[eventName]) {
//           this.middlewares[eventName] = [];
//       }
//       this.middlewares[eventName].push(middleware);
//   }

//   public async dispatch<T>(eventName: string, event: T): Promise<void> {
//       const middlewares = this.middlewares[eventName] || [];
//       const handlers = this.handlers[eventName] || [];

//       const executeMiddlewares = (index: number): Promise<void> | void => {
//           if (index < middlewares.length) {
//               const middleware = middlewares[index];
//               return middleware(event, () => executeMiddlewares(index + 1));
//           } else {
//               return executeHandlers();
//           }
//       };

//       const executeHandlers = async (): Promise<void> => {
//           for (const handler of handlers) {
//               await handler.handle(event);
//           }
//       };

//       await executeMiddlewares(0);
//   }
// }