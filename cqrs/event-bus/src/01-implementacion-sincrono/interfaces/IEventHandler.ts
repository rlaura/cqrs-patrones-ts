// EventBus.ts
export interface IEventHandler<T> {
  handle(event: T): void;
}
