import { IEventHandler } from "./IEventHandler";

export class EventBus {
  private handlers: { [eventName: string]: IEventHandler<any>[] } = {};

  public register<T>(eventName: string, handler: IEventHandler<T>): void {
    if (!this.handlers[eventName]) {
      this.handlers[eventName] = [];
    }
    this.handlers[eventName].push(handler);
  }

  public dispatch<T>(eventName: string, event: T): void {
    const handlers = this.handlers[eventName];
    if (handlers) {
      handlers.forEach((handler) => handler.handle(event));
    }
  }
}
