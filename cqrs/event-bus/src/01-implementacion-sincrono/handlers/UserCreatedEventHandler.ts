import { UserCreatedEvent } from "../events/UserCreatedEvent";
import { IEventHandler } from "../interfaces/IEventHandler";

export class UserCreatedEventHandler implements IEventHandler<UserCreatedEvent> {
  handle(event: UserCreatedEvent): void {
      console.log(`Usuario creado con ID: ${event.userId} y Nombre: ${event.name}`);
      // Lógica adicional aquí
  }
}