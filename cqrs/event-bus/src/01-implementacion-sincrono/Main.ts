import { OrderPlacedEvent } from "./events/OrderPlacedEvent";
import { UserCreatedEvent } from "./events/UserCreatedEvent";
import { OrderPlacedEventHandler } from "./handlers/OrderPlacedEventHandler";
import { UserCreatedEventHandler } from "./handlers/UserCreatedEventHandler";
import { EventBus } from "./interfaces/EventBus";
/**
 * implementacion de un event bus sincrono
 */
async function main() {
  const eventBus = new EventBus();

  // Registro de manejadores de eventos
  const userCreatedHandler = new UserCreatedEventHandler();
  eventBus.register("UserCreatedEvent", userCreatedHandler);

  const orderPlacedHandler = new OrderPlacedEventHandler();
  eventBus.register("OrderPlacedEvent", orderPlacedHandler);

  // Dispatching events
  const userCreatedEvent = new UserCreatedEvent("123", "Marco ");
  eventBus.dispatch("UserCreatedEvent", userCreatedEvent);

  const orderPlacedEvent = new OrderPlacedEvent("456", "123");
  eventBus.dispatch("OrderPlacedEvent", orderPlacedEvent);
}

main();